package uz.grepit.demo.service;

import uz.grepit.demo.dto.BookDto;
import uz.grepit.demo.dto.BookForm;

import java.util.List;

public interface BookService {

    BookDto addNewBook(BookForm newBook);

    List<BookDto> getBookByAuthor(String author);

    List<BookDto> getBookByName(String name);

    List<BookDto> getAll();

}
