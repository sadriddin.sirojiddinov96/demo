package uz.grepit.demo.service;

import org.springframework.stereotype.Service;
import uz.grepit.demo.dto.BookDto;
import uz.grepit.demo.dto.BookForm;
import uz.grepit.demo.entity.Book;
import uz.grepit.demo.exception.BookNotFoundExceptionByThisAuthor;
import uz.grepit.demo.repository.BookRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository repository;

    public BookServiceImpl(BookRepository repository) {
        this.repository = repository;
    }

    @Override
    public BookDto addNewBook(BookForm newBook) {

        Book book = repository.save(BookForm.toBook(newBook));
        return BookDto.fromBook(book);
    }

    @Override
    public List<BookDto> getBookByAuthor(String author) {
        List<Book> bookList = repository.findAllByAuthor(author);
        if (bookList.isEmpty())throw new BookNotFoundExceptionByThisAuthor("There is no book with this AUTHOR");
        return bookList.stream().map(BookDto::fromBook).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getBookByName(String name) {
        List<Book> bookList = repository.findAllByName(name);
        if (bookList.isEmpty())throw  new BookNotFoundExceptionByThisAuthor("There are not any books with this NAME ");
        return bookList.stream().map(BookDto::fromBook).collect(Collectors.toList());
    }

    @Override
    public List<BookDto> getAll() {
        return repository.findAll().stream().map(BookDto::fromBook).collect(Collectors.toList());
    }
}
