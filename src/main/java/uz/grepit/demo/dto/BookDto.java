package uz.grepit.demo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import uz.grepit.demo.entity.Book;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookDto {

    public static BookDto fromBook(Book book){
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setName(book.getName());
        bookDto.setPublishedDate(book.getPublishedDate());
        bookDto.setDescription(book.getDescription());
        bookDto.setAuthor(book.getAuthor());
        bookDto.setLanguage(book.getLanguage());
        return bookDto;
    }


    private Long id;

    private String name;

    private String publishedDate;

    private String description;

    private String author;

    private String language;


}
