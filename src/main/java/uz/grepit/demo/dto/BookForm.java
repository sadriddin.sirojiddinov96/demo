package uz.grepit.demo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import uz.grepit.demo.entity.Book;
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookForm {

    public static Book toBook(BookForm bookForm){
        Book book = new Book();
        if (bookForm.name == null)throw  new NullPointerException("Book Name cannot be NuLL");
        book.setName(bookForm.name);
        book.setPublishedDate(bookForm.publishedDate);
        book.setDescription(bookForm.description);
        book.setAuthor(bookForm.author);
        book.setLanguage(bookForm.language);

        return book;
    }

    public BookForm() {
    }

    private String name;

    private String publishedDate;

    private String description;

    private String author;

    private String language;

}
