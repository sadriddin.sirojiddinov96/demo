package uz.grepit.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class BookNotFoundExceptionByThisAuthor extends RuntimeException  {

    public BookNotFoundExceptionByThisAuthor() {
        super();
    }

    public BookNotFoundExceptionByThisAuthor(String message, Throwable cause) {
        super(message, cause);
    }
    public BookNotFoundExceptionByThisAuthor(String message) {
        super(message);
    }

    public BookNotFoundExceptionByThisAuthor(Throwable cause) {
        super(cause);
    }
}
