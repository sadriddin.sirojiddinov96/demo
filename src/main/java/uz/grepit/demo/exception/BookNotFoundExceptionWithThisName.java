package uz.grepit.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class BookNotFoundExceptionWithThisName extends RuntimeException {

    public BookNotFoundExceptionWithThisName() {
        super();
    }

    public BookNotFoundExceptionWithThisName(String message, Throwable cause) {
        super(message, cause);
    }
    public BookNotFoundExceptionWithThisName(String message) {
        super(message);
    }

    public BookNotFoundExceptionWithThisName(Throwable cause) {
        super(cause);
    }

}
