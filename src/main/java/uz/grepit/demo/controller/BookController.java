package uz.grepit.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.grepit.demo.dto.BookForm;
import uz.grepit.demo.service.BookService;

@RestController
@RequestMapping("api/v1/book")
public class BookController {

    private final BookService service;

    public BookController(BookService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity getAll(){
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping
    public ResponseEntity addNewBook(@RequestBody BookForm bookForm) {
        return ResponseEntity.ok(service.addNewBook(bookForm));
    }

    @GetMapping("{author}")
    public ResponseEntity getBooksByAuthor(@PathVariable String author){
        return ResponseEntity.ok(service.getBookByAuthor(author));
    }

    @PostMapping("{name}")
    public ResponseEntity getBooksByName(@PathVariable String name){
        return ResponseEntity.ok(service.getBookByName(name));
    }

}
