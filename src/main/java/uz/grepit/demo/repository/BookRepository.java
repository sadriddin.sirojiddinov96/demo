package uz.grepit.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.grepit.demo.entity.Book;

import java.util.List;

public interface BookRepository extends JpaRepository<Book,Long> {

    List<Book> findAllByAuthor(String author);

    List<Book> findAllByName(String name);

}
